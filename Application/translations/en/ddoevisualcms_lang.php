<?php
/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2017
 * @version   OXID eSales Visual CMS
 */

$sLangName = 'English';

$aLang = array(
    // Charsets
    'charset' => 'UTF-8',

    'DD_VISUAL_EDITOR_WIDGET_TABS_TOP'  => 'Bestseller',
    'DD_VISUAL_EDITOR_WIDGET_TABS_NEW'  => 'New arrivals',

    'DD_VISUAL_EDITOR_WIDGET_TREEVIEW_DEFAULT_TITLE'  => 'Treeview',
);