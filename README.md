# Branches explained

* `master` - compatible with OXID eShop master branch
* `b-3.x` - compatible with OXID eShop compilations 6.0 to 6.2

# Documentation

* [The repository with documentation](https://github.com/OXID-eSales/vcms-documentation)
* [Generated documentation](https://docs.oxid-esales.com/modules/vcms/de/3.5/)

# Bugs and issues

If you experience any bugs or issues, please report them in the section **Module Visual CMS** of https://bugs.oxid-esales.com.
