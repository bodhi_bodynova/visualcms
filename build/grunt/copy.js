/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2017
 * @version   OXID eSales Visual CMS
 */

module.exports = {

    ddvisualeditor: {
        expand: true,
        cwd: 'build/vendor/summernote/dist/font/',
        src: '*',
        dest: 'out/src/css/font/'
    }
}