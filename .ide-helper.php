<?php
namespace OxidEsales\VisualCmsModule\Core;
class ViewConfig extends \OxidEsales\Eshop\Core\ViewConfig{}
class Utils extends \OxidEsales\Eshop\Core\Utils{}
class UtilsView extends \OxidEsales\Eshop\Core\UtilsView{}
class Theme extends \OxidEsales\Eshop\Core\Theme{}
class Language extends \OxidEsales\Eshop\Core\Language{}

namespace OxidEsales\VisualCmsModule\Application\Controller;
class ContentController extends \OxidEsales\Eshop\Application\Controller\ContentController{}

namespace OxidEsales\VisualCmsModule\Application\Component;
class BasketComponent extends \OxidEsales\Eshop\Application\Component\BasketComponent{}

namespace OxidEsales\VisualCmsModule\Application\Model;
class Content extends \OxidEsales\Eshop\Application\Model\Content{}
class ContentList extends \OxidEsales\Eshop\Application\Model\ContentList{}
class SeoEncoderContent extends \OxidEsales\Eshop\Application\Model\SeoEncoderContent{}

namespace OxidEsales\VisualCmsModule\Application\Controller;
class VisualCmsLangJs extends \OxidEsales\Eshop\Application\Controller\FrontendController{}
class PreviewController extends \OxidEsales\VisualCmsModule\Application\Controller\ContentController{}
class CronController extends \OxidEsales\Eshop\Application\Controller\FrontendController{}


namespace OxidEsales\VisualCmsModule\Application\Controller\Admin;
class VisualCmsAdmin extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController{}
class VisualCmsMedia extends \OxidEsales\Eshop\Application\Controller\Admin\AdminDetailsController{}
