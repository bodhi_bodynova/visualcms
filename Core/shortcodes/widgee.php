<?php
use OxidEsales\VisualCmsModule\Application\Model\VisualEditorShortcode;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Component\Widget\ArticleBox;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Application\Model\ArticleList;

class widgee_shortcode extends VisualEditorShortcode {

    protected $_sTitle ='Widgee';

    protected $_sBackgroundColor = '#3ded33';

    protected $_sIcon = 'fa-bug';

    protected $_sShortCode = 'widgee';

    public function install(){

        // $this->setShortCode( basename( __FILE__, '.php' ) );

        $aCategories = $this->getCategories();

        $oLang = Registry::getLang();
        $this->setOptions(
            array(
                'categoryid'   => array(
                    'type'        => 'select',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CATEGORY' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CHOOSE_CATEGORY' ),
                    'values'      => $aCategories,
                    'dataFields'  => array(
                        'name' => 'label'
                    )
                ),
                /*  'id'   => array(
                      'data'        => 'searchArticle',
                      'type'        => 'select',
                      'label'       => 'Artikel',
                      'placeholder' => 'Bitte wählen Sie einen Artikel aus',
                      'dataFields'  => array(
                          'name' => 'label'
                      )
                  ), */
                'interval'         => array(
                    'type'   => 'select',
                    'label'  => 'Interval',
                    'values' => array(
                        2000  => 2  . ' Sekunden',
                        4000  => 4  . ' Sekunden',
                        6000  => 6  . ' Sekunden',
                        8000  => 8  . ' Sekunden',
                        10000 => 10 . ' Sekunden',
                        12000 => 12 . ' Sekunden',
                        10 => 'Quick as Lightning',
                    ),
                    'value' => 2000
                ),
                'name' => array(
                    'type'    => 'hidden',
                    'preview' => true
                ),
                'count' => array(
                    'type' => 'select',
                    'label' => 'Anzahl',
                    'values' => array(
                        10 => '10 Artikel',
                        20 => '20 Artikel',
                        30 => '30 Artikel',
                    ),
                    'value' => 10
                ),
                'shuffle' => array(
                    'type' => 'checkbox',
                    'label' => 'mischen, um die Artikel in einer zufälligen Reihenfolge anzeigen zu lassen,<br> ansonsten wird die Sortierung der Kategorie übernommen'
                )
            )
        );
    }

    public function parse($sContent='',$aParams = array()){

        if(!$aParams['categoryid']){
            return null;
        }

        // Karussell-ID zum ansteuern
        $sCarouselId = 'dd_carousel_' . md5( uniqid() );
        $sCarouselIdMD = 'dd_carousel_' . md5( uniqid() );
        $sCarouselIdSM = 'dd_carousel_' . md5( uniqid() );
        $sCarouselIdXS = 'dd_carousel_' . md5( uniqid() );
        if($aParams[ 'interval' ] == ""){
            $aParams[ 'interval' ] = 4000;
        }
        $aParams['interval'] = 6000;

        // Anzahl Artikel
        $Anzahl = $aParams['count'];

        $AnzahlBulletsLG = $Anzahl / 5;
        $AnzahlBulletsSM = $Anzahl / 2;
        // Bestimme Artikel aus Kategorie, bekomme ein Array mit Ergebnissen zurück
        $arrArtikel = $this->_getArticleId( $aParams);

        // Mische Array, um zufällige Produkte zu zeigen
        if($aParams['shuffle']){
            shuffle($arrArtikel);
        }

        // Für die angegebene Anzahl werden Variablen mit Artikel-ID befüllt
        for( $i=0; $i<$Anzahl; $i++ ){
            ${'variable'.$i} = $arrArtikel[$i];
        }

        // Karussell-Styles
        $this->addInlineStyle( "
            .carousel-control > .fa {
                top: 50%;
                position: absolute;
                transform: translateY(-50%);
                font-size: 2em;
            }
        ", true );

        $sOutput = '';
        $Item = '';
        $ItemMD = '';
        $ItemSM = '';
        $ItemXS = '';
        $bulletPoints = '';
        $bulletPointsSM = '';
        $bulletPointsXS = '';
        // boolean, um das aktive Item für das Karussell zu setzen.
        $true = true;
        $uniqueID = md5(uniqid());
        $uniqueIDSM = md5(uniqid());
        $uniqueIDXS = md5(uniqid());

        for($i=0;$i<($Anzahl/5);$i++){
            // für aktuellen Bulletpoint : <span' . ( $true ? ' style="background-color:#60a3b4;"' : '' ) . '></span>

            $active = "active";

            $bulletPoints .= '<button id="buttonLG' . $i . $uniqueID . '" onclick="changeActive(\'LG\',' . $i . ',\''.$uniqueID.'\')" style="background-color:transparent; border:0; width:64px;" role="button" class="owl-dot '.($i == 0 ? $active : null).'"><span></span></button>';
            $buttonCountLG = $i+1;
            if($buttonCountLG == $AnzahlBulletsLG){
                $buttonCountLG = 0;
            }
            $Item .= '<div idUnique="' . $uniqueID . '" idButton="'. $buttonCountLG .'" id="carousel' . $i . $uniqueID .  '"class="hidden-sm hidden-xs item' . ( $true ? ' active' : '' ) . '">';

            /**
             * GROßER VIEWPORT
             */

            $Item .= '<div class="">';

            for($j=$i*5;$j<5*$i+5;$j++) {
                $sClass= "";
                //if($j==0||$j==5||$j==10|| $j==15||$j==20 || $j==25 || $j==30){
                if($j%5 == 0){
                    $sClass= "col-md-offset-1";
                } else {
                    $sClass="";
                }
                $Item .=
                    '<div id="'. $j .'" class="col-md-2 ' . $sClass . '">     
                            <div class="boxwrapper">   
                                 <div class="list-container">
                                    <div class="row gridView">
                                         <div id="test" class="dd-shortcode-article productData productBox">
                                            [{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . ${'variable'.$j} . '"  isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_grid" inlist=1 skipESIforUser=1}]
                                         </div>
                                    </div>
                                 </div>
                             </div>
                          </div>';
            }

            $Item .= '</div>';

            /**
             * MITTEL VIEWPORT
             */

            $Item .= '</div>';

            $true = false;
        }
        $true = true;
        /*
        for($i=0;$i<($Anzahl/3);$i++){

            $ItemMD .= '<div id="carousel" class=" hidden-lg hidden-sm hidden-xs item' . ( $true ? ' active' : '' ) . '" style="height:400px">';

            $ItemMD .= '<div class="">';

            for($j=$i*3;$j<3*$i+3;$j++) {

                $ItemMD .=
                    '<div id="'. $j .'" class="col-md-4">
                            <div id="test" class="dd-shortcode-article productData productBox" style="left:100px;">
                                [{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . ${'variable'.$j} . '"  isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_grid" inlist=1 skipESIforUser=1}]
                             </div>
                          </div>';
            }

            $ItemMD .= '</div>';

            $ItemMD .= '</div>';

            $true = false;
        }
        $true = true;
        */
        for($i=0;$i<($Anzahl/2);$i++){

            $bulletPointsSM .= '<button id="buttonSM' . $i . $uniqueIDSM . '" onclick="changeActive(\'SM\',' . $i . ',\''. $uniqueIDSM.'\')" style="background-color:transparent; border:0; width:64px;" role="button" class="owl-dot"><span></span></button>';

            $buttonCountSM = $i+1;
            if($buttonCountSM == $AnzahlBulletsSM){
                $buttonCountSM = 0;
            }

            $ItemSM .= '<div idUnique="' . $uniqueIDSM . '" idButton="'.$buttonCountSM.'" id="carouselSM' . $i . $uniqueIDSM . '"class="hidden-md hidden-lg item' . ( $true ? ' active' : '' ) . '">';

            /**
             * GROßER VIEWPORT
             */

            $ItemSM .= '<div class="">';

            for($j=$i*2;$j<2*$i+2;$j++) {

                $ItemSM .=
                    '<div id="'. $j .'" class="col-sm-6">     
                            <div class="boxwrapper">   
                                 <div class="list-container">
                                    <div class="row gridView">
                                         <div id="test" class="dd-shortcode-article productData productBox">
                                            [{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . ${'variable'.$j} . '"  isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_grid" inlist=1 skipESIforUser=1}]
                                         </div>
                                    </div>
                                 </div>
                             </div>
                          </div>';
            }

            $ItemSM .= '</div>';
            /**
             * MITTEL VIEWPORT
             */

            $ItemSM .= '</div>';

            $true = false;
        }
        $true = true;

        for($i=0;$i<($Anzahl);$i++){

            $bulletPointsXS .= '<button id="buttonXS' . $i . '" onclick="changeActive(\'XS\',' . $i . ',\''.$uniqueIDXS.'\')" style="background-color:transparent; border:0; width:64px;" role="button" class="owl-dot"><span></span></button>';

            $ItemXS .= '<div id="carouselXS' . $i . $uniqueIDXS . '"class="hidden-md hidden-lg hidden-sm item' . ( $true ? ' active' : '' ) . '">';

            /**
             * XS VIEWPORT
             */

            $ItemXS .= '<div class="">';

            for($j=$i*1;$j<1*$i+1;$j++) {

                $ItemXS .=
                    '<div id="'. $j .'" class="col-xs-12">     
                            <div class="boxwrapper xsSlider">   
                                 <div class="list-container">
                                    <div class="row gridView">
                                         <div id="test" class="dd-shortcode-article productData productBox">
                                            [{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . ${'variable'.$j} . '"  isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_grid" inlist=1 skipESIforUser=1}]
                                         </div>
                                    </div>
                                 </div>
                             </div>
                          </div>';
            }

            $ItemXS .= '</div>';
            /**
             * MITTEL VIEWPORT
             */

            $ItemXS .= '</div>';

            $true = false;
        }

        /**
         * Karussell für den LG/MD-Viewport.
         * Danach für SM und XS.
         * TODO: prüfe, ob slide left active! Wenn ja, setze timeout funktion.
         */

        // Falls kleine Bulletpoints erwünscht sind : <div class="hidden-md hidden-lg hidden-sm owl-dots">' . $bulletPointsXS . '</div>

        $sOutput .= '
            [{*oxscript add="$(document).ready(function(){
                $(\'.dd-carousel-slider\').on(slide,function(){
                    window.scrollBy(0,1);
                });
            });"*}]
                     
         <div class=" hidden-sm hidden-xs dd-shortcode-carousel dd-carousel-slider">          
             <div id="' . $sCarouselId . '" class="carousel slide" data-ride="carousel"  data-pause="hover" data-interval="'. $aParams[ 'interval' ] .'">
                <div class="carousel-inner" role ="listbox">'

            . $Item .

            '</div>    
                <div hover="hoverowl();" id="owlbutton" class="owl-theme owl-loaded owl-drag">
          <div class="hidden-xs hidden-sm owl-dots">' . $bulletPoints . '</div></div>
              </div>
              
          </div>
          
          <div class="hidden-lg hidden-md hidden-xs dd-shortcode-carousel dd-carousel-slider">          
             <div id="' . $sCarouselIdSM . '" class="carousel slide" data-ride="carousel"  data-pause="hover" data-interval="'. $aParams[ 'interval' ] .'">
                <div class="carousel-inner" role ="listbox">'

            . $ItemSM .

            '</div>   
                 <div hover="hoverowl();" id="owlbutton" class="owl-theme owl-loaded owl-drag">
          <div class="hidden-md hidden-lg hidden-xs owl-dots">' . $bulletPointsSM . '</div></div> 
              </div>
                       
          </div>
          <div class="hidden-lg hidden-md hidden-sm dd-shortcode-carousel dd-carousel-slider">          
             <div id="' . $sCarouselIdXS . '" class="carousel slide" data-ride="carousel"  data-pause="hover" data-interval="'. $aParams[ 'interval' ] .'">
                <div class="carousel-inner" role ="listbox">'

            . $ItemXS .

            '</div>    
              </div>
          </div>
          
          <a class=" hidden-xs hidden-sm left carousel-control" href="#'. $sCarouselId .'" role="button" data-slide="prev" style="width:10% !important; background-color:transparent;float:left !important; padding-top: 207px ">
              <i class="fal fa-chevron-left fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
          </a>
                 
          <a class=" hidden-xs hidden-sm right carousel-control" href="#'. $sCarouselId .'" role="button" data-slide="next" style="width:10% !important; background-color:transparent;float:right !important; padding-top: 207px ">
              <i class="fal fa-chevron-right fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
          </a>
          
          <a class=" hidden-xs hidden-md hidden-lg left carousel-control" href="#'. $sCarouselIdSM .'" role="button" data-slide="prev" style="width:10% !important; background-color:transparent;float:left !important; padding-top: 207px ">
              <i class="fal fa-chevron-left fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
          </a>
                 
          <a class=" hidden-xs hidden-md hidden-lg right carousel-control" href="#'. $sCarouselIdSM .'" role="button" data-slide="next" style="width:10% !important; background-color:transparent;float:right !important; padding-top: 207px ">
              <i class="fal fa-chevron-right fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
          </a>
          <a class=" hidden-sm hidden-md hidden-lg left carousel-control" href="#'. $sCarouselIdXS .'" role="button" data-slide="prev" style="height:65% !important;width:25% !important; background-color:transparent;float:left !important; padding-top: 207px ">
              <i class="fal fa-chevron-left fa-2x" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
          </a>
                 
          <a class=" hidden-sm hidden-md hidden-lg right carousel-control" href="#'. $sCarouselIdXS .'" role="button" data-slide="next" style="height:65% !important;width:25% !important; background-color:transparent;float:right !important; padding-top: 207px ">
              <i class="fal fa-chevron-right fa-2x" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
          </a>
          
                <script>
                    function changeActive(s,x,uni){
                        
                        if(s ==="SM"){
                            for(var i = 0; i<16;i++){
                                if(i!=x){
                                    if(i<='.$AnzahlBulletsSM.'){
                                        $("#buttonSM"+i).removeClass("active");
                                    }
                                    $("#carouselSM" + i + uni + "" ).removeClass("active");  
                                } else {
                                    if(i<='.$AnzahlBulletsSM.'){
                                        $("#buttonSM"+i).addClass("active");
                                    }
                                    $("#carouselSM" + i + uni + "" ).addClass("active");  
                                }
                            }
                        } else if(s ==="LG"){
                            for(var i = 0; i<7;i++){
                                
                                if(i!=x){
                                    if(i<='.$AnzahlBulletsLG.'){
                                        $("#buttonLG"+i+uni).removeClass("active");
                                    }
                                    //console.log("carousel"+i+uni);
                                    $("#carousel" + i + uni + "" ).removeClass("active");  
                                } else {
                                    if(i<='.$AnzahlBulletsLG.'){
                                        $("#buttonLG"+i+uni).addClass("active");
                                    }                                    
                                    //console.log("carousel"+i+uni);
                                    $("#carousel" + i + uni +"" ).addClass("active");  
                                }
                            }
                        }
                    }
                    
                    var myVar = setInterval(function(){window.scrollBy(0,1); window.scrollBy(0,-1)},500);
                    setTimeout(function(){clearInterval(myVar);},' . ($aParams['interval'] * (3*($aParams['count']/8))) . ');
                    
                    window.onload = function(){
                                        (function(e){var o="left",n="right",d="up",v="down",c="in",w="out",l="none",r="auto",k="swipe",s="pinch",x="tap",i="doubletap",b="longtap",A="horizontal",t="vertical",h="all",q=10,f="start",j="move",g="end",p="cancel",a="ontouchstart" in window,y="TouchSwipe";var m={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,triggerOnTouchEnd:true,triggerOnTouchLeave:false,allowPageScroll:"auto",fallbackToMouseEvents:true,excludedElements:"button, input, select, textarea, span, .noSwipe, .xsSlider"};e.fn.swipe=function(D){var C=e(this),B=C.data(y);if(B&&typeof D==="string"){if(B[D]){return B[D].apply(this,Array.prototype.slice.call(arguments,1))}else{e.error("Method "+D+" does not exist on jQuery.swipe")}}else{if(!B&&(typeof D==="object"||!D)){return u.apply(this,arguments)}}return C};e.fn.swipe.defaults=m;e.fn.swipe.phases={PHASE_START:f,PHASE_MOVE:j,PHASE_END:g,PHASE_CANCEL:p};e.fn.swipe.directions={LEFT:o,RIGHT:n,UP:d,DOWN:v,IN:c,OUT:w};e.fn.swipe.pageScroll={NONE:l,HORIZONTAL:A,VERTICAL:t,AUTO:r};e.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,ALL:h};function u(B){if(B&&(B.allowPageScroll===undefined&&(B.swipe!==undefined||B.swipeStatus!==undefined))){B.allowPageScroll=l}if(B.click!==undefined&&B.tap===undefined){B.tap=B.click}if(!B){B={}}B=e.extend({},e.fn.swipe.defaults,B);return this.each(function(){var D=e(this);var C=D.data(y);if(!C){C=new z(this,B);D.data(y,C)}})}function z(a0,aq){var av=(a||!aq.fallbackToMouseEvents),G=av?"touchstart":"mousedown",au=av?"touchmove":"mousemove",R=av?"touchend":"mouseup",P=av?null:"mouseleave",az="touchcancel";var ac=0,aL=null,Y=0,aX=0,aV=0,D=1,am=0,aF=0,J=null;var aN=e(a0);var W="start";var T=0;var aM=null;var Q=0,aY=0,a1=0,aa=0,K=0;var aS=null;try{aN.bind(G,aJ);aN.bind(az,a5)}catch(ag){e.error("events not supported "+G+","+az+" on jQuery.swipe")}this.enable=function(){aN.bind(G,aJ);aN.bind(az,a5);return aN};this.disable=function(){aG();return aN};this.destroy=function(){aG();aN.data(y,null);return aN};this.option=function(a8,a7){if(aq[a8]!==undefined){if(a7===undefined){return aq[a8]}else{aq[a8]=a7}}else{e.error("Option "+a8+" does not exist on jQuery.swipe.options")}};function aJ(a9){if(ax()){return}if(e(a9.target).closest(aq.excludedElements,aN).length>0){return}var ba=a9.originalEvent?a9.originalEvent:a9;var a8,a7=a?ba.touches[0]:ba;W=f;if(a){T=ba.touches.length}else{a9.preventDefault()}ac=0;aL=null;aF=null;Y=0;aX=0;aV=0;D=1;am=0;aM=af();J=X();O();if(!a||(T===aq.fingers||aq.fingers===h)||aT()){ae(0,a7);Q=ao();if(T==2){ae(1,ba.touches[1]);aX=aV=ap(aM[0].start,aM[1].start)}if(aq.swipeStatus||aq.pinchStatus){a8=L(ba,W)}}else{a8=false}if(a8===false){W=p;L(ba,W);return a8}else{ak(true)}}function aZ(ba){var bd=ba.originalEvent?ba.originalEvent:ba;if(W===g||W===p||ai()){return}var a9,a8=a?bd.touches[0]:bd;var bb=aD(a8);aY=ao();if(a){T=bd.touches.length}W=j;if(T==2){if(aX==0){ae(1,bd.touches[1]);aX=aV=ap(aM[0].start,aM[1].start)}else{aD(bd.touches[1]);aV=ap(aM[0].end,aM[1].end);aF=an(aM[0].end,aM[1].end)}D=a3(aX,aV);am=Math.abs(aX-aV)}if((T===aq.fingers||aq.fingers===h)||!a||aT()){aL=aH(bb.start,bb.end);ah(ba,aL);ac=aO(bb.start,bb.end);Y=aI();aE(aL,ac);if(aq.swipeStatus||aq.pinchStatus){a9=L(bd,W)}if(!aq.triggerOnTouchEnd||aq.triggerOnTouchLeave){var a7=true;if(aq.triggerOnTouchLeave){var bc=aU(this);a7=B(bb.end,bc)}if(!aq.triggerOnTouchEnd&&a7){W=ay(j)}else{if(aq.triggerOnTouchLeave&&!a7){W=ay(g)}}if(W==p||W==g){L(bd,W)}}}else{W=p;L(bd,W)}if(a9===false){W=p;L(bd,W)}}function I(a7){var a8=a7.originalEvent;if(a){if(a8.touches.length>0){C();return true}}if(ai()){T=aa}a7.preventDefault();aY=ao();Y=aI();if(a6()){W=p;L(a8,W)}else{if(aq.triggerOnTouchEnd||(aq.triggerOnTouchEnd==false&&W===j)){W=g;L(a8,W)}else{if(!aq.triggerOnTouchEnd&&a2()){W=g;aB(a8,W,x)}else{if(W===j){W=p;L(a8,W)}}}}ak(false)}function a5(){T=0;aY=0;Q=0;aX=0;aV=0;D=1;O();ak(false)}function H(a7){var a8=a7.originalEvent;if(aq.triggerOnTouchLeave){W=ay(g);L(a8,W)}}function aG(){aN.unbind(G,aJ);aN.unbind(az,a5);aN.unbind(au,aZ);aN.unbind(R,I);if(P){aN.unbind(P,H)}ak(false)}function ay(bb){var ba=bb;var a9=aw();var a8=aj();var a7=a6();if(!a9||a7){ba=p}else{if(a8&&bb==j&&(!aq.triggerOnTouchEnd||aq.triggerOnTouchLeave)){ba=g}else{if(!a8&&bb==g&&aq.triggerOnTouchLeave){ba=p}}}return ba}function L(a9,a7){var a8=undefined;if(F()||S()){a8=aB(a9,a7,k)}else{if((M()||aT())&&a8!==false){a8=aB(a9,a7,s)}}if(aC()&&a8!==false){a8=aB(a9,a7,i)}else{if(al()&&a8!==false){a8=aB(a9,a7,b)}else{if(ad()&&a8!==false){a8=aB(a9,a7,x)}}}if(a7===p){a5(a9)}if(a7===g){if(a){if(a9.touches.length==0){a5(a9)}}else{a5(a9)}}return a8}function aB(ba,a7,a9){var a8=undefined;if(a9==k){aN.trigger("swipeStatus",[a7,aL||null,ac||0,Y||0,T]);if(aq.swipeStatus){a8=aq.swipeStatus.call(aN,ba,a7,aL||null,ac||0,Y||0,T);if(a8===false){return false}}if(a7==g&&aR()){aN.trigger("swipe",[aL,ac,Y,T]);if(aq.swipe){a8=aq.swipe.call(aN,ba,aL,ac,Y,T);if(a8===false){return false}}switch(aL){case o:aN.trigger("swipeLeft",[aL,ac,Y,T]);if(aq.swipeLeft){a8=aq.swipeLeft.call(aN,ba,aL,ac,Y,T)}break;case n:aN.trigger("swipeRight",[aL,ac,Y,T]);if(aq.swipeRight){a8=aq.swipeRight.call(aN,ba,aL,ac,Y,T)}break;case d:aN.trigger("swipeUp",[aL,ac,Y,T]);if(aq.swipeUp){a8=aq.swipeUp.call(aN,ba,aL,ac,Y,T)}break;case v:aN.trigger("swipeDown",[aL,ac,Y,T]);if(aq.swipeDown){a8=aq.swipeDown.call(aN,ba,aL,ac,Y,T)}break}}}if(a9==s){aN.trigger("pinchStatus",[a7,aF||null,am||0,Y||0,T,D]);if(aq.pinchStatus){a8=aq.pinchStatus.call(aN,ba,a7,aF||null,am||0,Y||0,T,D);if(a8===false){return false}}if(a7==g&&a4()){switch(aF){case c:aN.trigger("pinchIn",[aF||null,am||0,Y||0,T,D]);if(aq.pinchIn){a8=aq.pinchIn.call(aN,ba,aF||null,am||0,Y||0,T,D)}break;case w:aN.trigger("pinchOut",[aF||null,am||0,Y||0,T,D]);if(aq.pinchOut){a8=aq.pinchOut.call(aN,ba,aF||null,am||0,Y||0,T,D)}break}}}if(a9==x){if(a7===p||a7===g){clearTimeout(aS);if(V()&&!E()){K=ao();aS=setTimeout(e.proxy(function(){K=null;aN.trigger("tap",[ba.target]);if(aq.tap){a8=aq.tap.call(aN,ba,ba.target)}},this),aq.doubleTapThreshold)}else{K=null;aN.trigger("tap",[ba.target]);if(aq.tap){a8=aq.tap.call(aN,ba,ba.target)}}}}else{if(a9==i){if(a7===p||a7===g){clearTimeout(aS);K=null;aN.trigger("doubletap",[ba.target]);if(aq.doubleTap){a8=aq.doubleTap.call(aN,ba,ba.target)}}}else{if(a9==b){if(a7===p||a7===g){clearTimeout(aS);K=null;aN.trigger("longtap",[ba.target]);if(aq.longTap){a8=aq.longTap.call(aN,ba,ba.target)}}}}}return a8}function aj(){var a7=true;if(aq.threshold!==null){a7=ac>=aq.threshold}return a7}function a6(){var a7=false;if(aq.cancelThreshold!==null&&aL!==null){a7=(aP(aL)-ac)>=aq.cancelThreshold}return a7}function ab(){if(aq.pinchThreshold!==null){return am>=aq.pinchThreshold}return true}function aw(){var a7;if(aq.maxTimeThreshold){if(Y>=aq.maxTimeThreshold){a7=false}else{a7=true}}else{a7=true}return a7}function ah(a7,a8){if(aq.allowPageScroll===l||aT()){a7.preventDefault()}else{var a9=aq.allowPageScroll===r;switch(a8){case o:if((aq.swipeLeft&&a9)||(!a9&&aq.allowPageScroll!=A)){a7.preventDefault()}break;case n:if((aq.swipeRight&&a9)||(!a9&&aq.allowPageScroll!=A)){a7.preventDefault()}break;case d:if((aq.swipeUp&&a9)||(!a9&&aq.allowPageScroll!=t)){a7.preventDefault()}break;case v:if((aq.swipeDown&&a9)||(!a9&&aq.allowPageScroll!=t)){a7.preventDefault()}break}}}function a4(){var a8=aK();var a7=U();var a9=ab();return a8&&a7&&a9}function aT(){return !!(aq.pinchStatus||aq.pinchIn||aq.pinchOut)}function M(){return !!(a4()&&aT())}function aR(){var ba=aw();var bc=aj();var a9=aK();var a7=U();var a8=a6();var bb=!a8&&a7&&a9&&bc&&ba;return bb}function S(){return !!(aq.swipe||aq.swipeStatus||aq.swipeLeft||aq.swipeRight||aq.swipeUp||aq.swipeDown)}function F(){return !!(aR()&&S())}function aK(){return((T===aq.fingers||aq.fingers===h)||!a)}function U(){return aM[0].end.x!==0}function a2(){return !!(aq.tap)}function V(){return !!(aq.doubleTap)}function aQ(){return !!(aq.longTap)}function N(){if(K==null){return false}var a7=ao();return(V()&&((a7-K)<=aq.doubleTapThreshold))}function E(){return N()}function at(){return((T===1||!a)&&(isNaN(ac)||ac===0))}function aW(){return((Y>aq.longTapThreshold)&&(ac<q))}function ad(){return !!(at()&&a2())}function aC(){return !!(N()&&V())}function al(){return !!(aW()&&aQ())}function C(){a1=ao();aa=event.touches.length+1}function O(){a1=0;aa=0}function ai(){var a7=false;if(a1){var a8=ao()-a1;if(a8<=aq.fingerReleaseThreshold){a7=true}}return a7}function ax(){return !!(aN.data(y+"_intouch")===true)}function ak(a7){if(a7===true){aN.bind(au,aZ);aN.bind(R,I);if(P){aN.bind(P,H)}}else{aN.unbind(au,aZ,false);aN.unbind(R,I,false);if(P){aN.unbind(P,H,false)}}aN.data(y+"_intouch",a7===true)}function ae(a8,a7){var a9=a7.identifier!==undefined?a7.identifier:0;aM[a8].identifier=a9;aM[a8].start.x=aM[a8].end.x=a7.pageX||a7.clientX;aM[a8].start.y=aM[a8].end.y=a7.pageY||a7.clientY;return aM[a8]}function aD(a7){var a9=a7.identifier!==undefined?a7.identifier:0;var a8=Z(a9);a8.end.x=a7.pageX||a7.clientX;a8.end.y=a7.pageY||a7.clientY;return a8}function Z(a8){for(var a7=0;a7<aM.length;a7++){if(aM[a7].identifier==a8){return aM[a7]}}}function af(){var a7=[];for(var a8=0;a8<=5;a8++){a7.push({start:{x:0,y:0},end:{x:0,y:0},identifier:0})}return a7}function aE(a7,a8){a8=Math.max(a8,aP(a7));J[a7].distance=a8}function aP(a7){return J[a7].distance}function X(){var a7={};a7[o]=ar(o);a7[n]=ar(n);a7[d]=ar(d);a7[v]=ar(v);return a7}function ar(a7){return{direction:a7,distance:0}}function aI(){return aY-Q}function ap(ba,a9){var a8=Math.abs(ba.x-a9.x);var a7=Math.abs(ba.y-a9.y);return Math.round(Math.sqrt(a8*a8+a7*a7))}function a3(a7,a8){var a9=(a8/a7)*1;return a9.toFixed(2)}function an(){if(D<1){return w}else{return c}}function aO(a8,a7){return Math.round(Math.sqrt(Math.pow(a7.x-a8.x,2)+Math.pow(a7.y-a8.y,2)))}function aA(ba,a8){var a7=ba.x-a8.x;var bc=a8.y-ba.y;var a9=Math.atan2(bc,a7);var bb=Math.round(a9*180/Math.PI);if(bb<0){bb=360-Math.abs(bb)}return bb}function aH(a8,a7){var a9=aA(a8,a7);if((a9<=45)&&(a9>=0)){return o}else{if((a9<=360)&&(a9>=315)){return o}else{if((a9>=135)&&(a9<=225)){return n}else{if((a9>45)&&(a9<135)){return v}else{return d}}}}}function ao(){var a7=new Date();return a7.getTime()}function aU(a7){a7=e(a7);var a9=a7.offset();var a8={left:a9.left,right:a9.left+a7.outerWidth(),top:a9.top,bottom:a9.top+a7.outerHeight()};return a8}function B(a7,a8){return(a7.x>a8.left&&a7.x<a8.right&&a7.y>a8.top&&a7.y<a8.bottom)}}})(jQuery);

                        $(".carousel").swipe({
                        swipe: function(event, direction) {
                            //console.log(direction);
                            if (direction == \'left\') $(this).carousel(\'next\');
                            if (direction == \'right\') $(this).carousel(\'prev\');
                            
                          },
                        allowPageScroll:"vertical",
                        click:true

                        });
                        $.fn.carousel.Constructor.prototype.next = function () {
                            this.slide(\'next\');
                            var childNodes = this.$element[0].children[0].childNodes;
                            //console.log(childNodes);
                            for(i=0;i<childNodes.length;i++){
                                if(childNodes[i].classList.contains(\'active\') && childNodes[i].classList.contains(\'hidden-xs\')){
                                    
                                    var uniqueLG = childNodes[i].attributes.idunique.nodeValue;
                                    var buttonID = childNodes[i].attributes.idbutton.nodeValue;
                                    var buttonIDVG = buttonID - 1;
                                    if(buttonID==0){
                                        buttonIDVG = '.($AnzahlBulletsLG-1).';
                                    }
                                    buttonID += uniqueLG;
                                    buttonIDVG += uniqueLG;
                                    $("#buttonLG"+(buttonIDVG)+"").removeClass("active");
                                    $("#buttonLG"+buttonID+"").addClass("active");
                                }
                                if(childNodes[i].classList.contains(\'active\') && childNodes[i].classList.contains(\'hidden-md\')  && !(childNodes[i].classList.contains(\'hidden-sm\'))){
                                    //console.log(childNodes[i]);
                                    var uniqueSM = childNodes[i].attributes.idunique.nodeValue;
                                    var buttonID = childNodes[i].attributes.idbutton.nodeValue;
                                    var buttonIDVG = buttonID - 1;
                                    if(buttonID==0){
                                        buttonIDVG = '.($AnzahlBulletsSM-1).';
                                    }
                                    buttonID += uniqueSM;
                                    buttonIDVG += uniqueSM;
                                    $("#buttonSM"+(buttonIDVG)+"").removeClass("active");
                                    $("#buttonSM"+buttonID+"").addClass("active");
                                }
                            }
                        };
                        $.fn.carousel.Constructor.prototype.prev = function () {
                            this.slide(\'prev\');
                            var childNodes = this.$element[0].children[0].childNodes;
                            for(i=0;i<childNodes.length;i++){
                                if(childNodes[i].classList.contains(\'active\') && childNodes[i].classList.contains(\'hidden-xs\')){
                                    //console.log(childNodes[i]);
                                    var uniqueLG = childNodes[i].attributes.idunique.nodeValue;

                                    
                                    var buttonID = parseInt(childNodes[i].attributes.idbutton.nodeValue);
                                    
                                    
                                    //console.log(buttonID);
                                    buttonID = (buttonID+'.($AnzahlBulletsLG-2).')%'.$AnzahlBulletsLG.';
                                    
                                    
                                    //console.log("add"+buttonID);
                                    var buttonIDVG = (buttonID + 1) % '.$AnzahlBulletsLG.';
                                    buttonID += uniqueLG;
                                    buttonIDVG += uniqueLG;
                                    //console.log("remove"+buttonIDVG);
                                    $("#buttonLG"+(buttonIDVG)+"").removeClass("active");
                                    $("#buttonLG"+buttonID+"").addClass("active");
                                }
                                if(childNodes[i].classList.contains(\'active\') && childNodes[i].classList.contains(\'hidden-md\') && !(childNodes[i].classList.contains(\'hidden-sm\'))){
                                    //console.log(childNodes[i]);
                                    var uniqueSM = childNodes[i].attributes.idunique.nodeValue;

                                    var buttonID = parseInt(childNodes[i].attributes.idbutton.nodeValue);
                                    //console.log(buttonID);
                                    buttonID = (buttonID+'.($AnzahlBulletsSM-2).')%'.$AnzahlBulletsSM.';
                                    
                                    
                                    //console.log("add"+buttonID);
                                    var buttonIDVG = (buttonID + 1) % '.$AnzahlBulletsSM.';
                                    buttonID += uniqueSM;
                                    buttonIDVG += uniqueSM;
                                    //console.log("remove"+buttonIDVG);
                                    $("#buttonSM"+(buttonIDVG)+"").removeClass("active");
                                    $("#buttonSM"+buttonID+"").addClass("active");
                                }
                            }
                        };
                        
                    };
                </script>
                    ';

        return $sOutput;
        //return $AnzahlBulletsLG;
    }

    // Steuerung für SM Viewport
    /*
      <a class="hidden-lg hidden-md hidden-xs left carousel-control" href="#'. $sCarouselIdSM .'" role="button" data-slide="prev" style="text-shadow:0 !important; color:#626261; !important; background-color:transparent;float:left; padding-top: 150px ">
              <i class="fal fa-chevron-left fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
          </a>

          <a class="hidden-lg hidden-md hidden-xs right carousel-control" href="#'. $sCarouselIdSM .'" role="button" data-slide="next" style="text-shadow:0 !important; color:#626261; !important; background-color:transparent;float:right; padding-top: 150px ">
              <i class="fal fa-chevron-right fa-3x" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
          </a>
     */


    public function searchArticle()
    {
        /** @var \OxidEsales\Eshop\Core\Config $oConfig */
        $oConfig = $this->getConfig();

        $aArticles = array();

        if( $oConfig->getRequestParameter( 'value' ) )
        {
            ///** @var Article $oProduct
            $oProduct = oxNew( Article::class );
            $oProduct->load( $oConfig->getRequestParameter( 'value' ) );

            $aArticles[] = array(
                'value'       => $oProduct->getId(),
                'label'       => $oProduct->oxarticles__oxtitle->value,
                'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                'icon'        => $oProduct->getIconUrl()
            );

        }
        elseif( $oConfig->getRequestParameter( 'search' ) )
        {
            $oDb = DatabaseProvider::getDb();
            $sSearch = $oConfig->getRequestParameter( 'search' );

            /** @var ArticleList $oList */
            $oList = oxNew( ArticleList::class );
            $sViewName = getViewName( 'oxarticles' );

            $sSelect = "SELECT
                            *
                        FROM " . $sViewName . "
                        WHERE (
                            `OXARTNUM` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . " OR
                            `OXTITLE` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . " OR
                            `OXEAN` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . "
                          )
                          AND `OXPARENTID` = ''
                        ";

            $oList->selectString( $sSelect );

            /** @var Article $oProduct */
            foreach( $oList as $oProduct )
            {
                $aArticles[] = array(
                    'value'       => $oProduct->getId(),
                    'label'       => $oProduct->oxarticles__oxtitle->value,
                    'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                    'icon'        => $oProduct->getIconUrl()
                );
            }
        }

        header( 'Content-Type: application/json' );
        Registry::getUtils()->showJsonAndExit( $aArticles );
    }

    protected function _getArticleId( $aParams = array() )
    {
        if( !$aParams )
        {
            return null;
        }
        $arrArticleId = $this->_findArticle( $aParams );
        return $arrArticleId;
    }

    protected function _findArticle( $aParams )
    {
        $oDb = DatabaseProvider::getDb( DatabaseProvider::FETCH_MODE_ASSOC );

        // Artikel mit gesetztem Flag aus den angegebenen Kategorien raussuchen

        $sortSQL = "SELECT OXDEFSORT,OXDEFSORTMODE FROM oxcategories WHERE OXID = ?";
        $aOne = $oDb->getRow($sortSQL,array($aParams['categoryid']));

        if($aOne['OXDEFSORTMODE'] == 0){
            $ascdesc = 'asc';
        } else {
            $ascdesc ='desc';
        }

        $sSQL = "SELECT
                     oa.oxid,
                     oa.oxstock,
                     oa.oxvarstock
                 FROM " . getViewName( 'oxarticles' ) . " AS oa
                     INNER JOIN oxobject2category AS o2c ON o2c.oxobjectid = oa.oxid
                     INNER JOIN oxcategories AS oc ON oc.oxid = o2c.oxcatnid
                 WHERE oa.oxactive = 1";

        // Kategorie
        if( $aParams[ 'categoryid' ] )
        {
            $sSQL .= " AND oc.oxid = " . $oDb->quote( $aParams[ 'categoryid' ] );
        }
        if($aOne['OXDEFSORT'] !== ""){
            $sSQL .= " ORDER BY oa." . $aOne['OXDEFSORT'] . " " . $ascdesc;
        } else {
            $sSQL .= " ORDER BY o2c.OXPOS ASC";
        }

        $aAll = $oDb->getAll( $sSQL );

        // Überprüfen, ob für den Artikel der Mindestbestand erreicht wird
        // Bei Vaterartikeln muss die Summe der Lagermengen der Varianten berücksichtigt werden
        $aSelected = Array();

        foreach ( $aAll AS $aArticle )
        {
            //  if ( $aArticle[ 'OXSTOCK' ] + $aArticle[ 'OXVARSTOCK' ] > (int)$aParams[ 'minstock' ] )
            // {
            $aSelected[] = $aArticle[ 'OXID' ];
            // }
        }
        return $aSelected;
    }
}