<?php

use OxidEsales\VisualCmsModule\Application\Model\VisualEditorShortcode;
use OxidEsales\VisualCmsModule\Application\Model\Media;

use OxidEsales\Eshop\Core\Registry;

class manufacturer_shortcode extends VisualEditorShortcode
{
    protected $_sTitle = 'ManufacturerSlider';

    protected $_sBackgroundColor = '#3ded33';

    protected $_sIcon = 'fa-sliders';

    protected $_sShortCode = 'widgee';

    public function install()
    {
        $this->setShortCode(basename(__FILE__, '.php'));
    }

    public function parse($sContent = '', $aParams = array())
    {
        $oSmarty = Registry::get('oxUtilsView')->getSmarty();
        $oSmarty->assign(
            array(
                'oView'     =>  $this->getConfig()->getActiveView(),
                'shortcode' =>  $this->getShortCode(),
            )
        );
        $sOutput =  '<div class="row">'.
                        '<div class="container-fluid">'.
                            '<div class="boxwrapper">'.
                                '<div class="">'.
                                        '[{foreach from=$oView->getManufacturerList() item=oManufacturer}]
                                            [{*$oManufacturer|var_dump*}]
                                            [{if $oManufacturer->oxmanufacturers__oxicon->value}]
                                                [{*$oManufacturer->getIconUrl()*}]
                                               <div class="col-xs-1">
                                                    <a style="height:60px;display:table-cell;vertical-align: middle;" href="[{$oManufacturer->getLink()}]" title="[{oxmultilang ident="VIEW_ALL_PRODUCTS"}]">
                                                        <img class="img-responsive" src="[{$oManufacturer->getIconUrl()}]" alt="[{$oManufacturer->oxmanufacturers__oxtitle->value}]">
                                                    </a>
                                                </div>
                                            [{/if}]
                                        [{/foreach}]'.
                                '</div>'.

                            '</div>'.
                        '</div>'.
                    '</div>';
        return $sOutput;
    }
}