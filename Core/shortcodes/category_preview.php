<?php

use OxidEsales\VisualCmsModule\Application\Model\VisualEditorShortcode;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Application\Component\Widget\ArticleBox;
use OxidEsales\Eshop\Application\Model\Article;
use OxidEsales\Eshop\Application\Model\ArticleList;

class category_preview_shortcode extends VisualEditorShortcode
{

    protected $_sTitle = 'MegaCat';

    protected $_sBackgroundColor = '#60a3b4';

    protected $_sIcon = 'fa-paw';

   // protected $_sShortCode = 'category_preview';


    public function install()
    {
        $this->setShortCode(basename(__FILE__, '.php'));

        $oLang = Registry::getLang();
        $aCategories = $this->getCategories();


        $this->setOptions(
            array(
                'categoryid'   => array(
                    'type'        => 'select',
                    'label'       => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CATEGORY' ),
                    'placeholder' => $oLang->translateString( 'DD_VISUAL_EDITOR_WIDGET_CHOOSE_CATEGORY' ),
                    'values'      => $aCategories,
                    'dataFields'  => array(
                        'name' => 'label'
                    )
                ),
                'count' => array(
                    'type' => 'select',
                    'label' => 'Anzahl',
                    'values' => array(
                        //6 => '6 Artikel',
                        12 => '12 Artikel',
                        //18 => '18 Artikel',
                        24 => '24 Artikel',
                        //30 => '30 Artikel',
                        36 => '36 Artikel',
                        //42 => '42 Artikel',
                        48 => '48 Artikel',
                        120 => 'alle(maximal 120)'
                    ),
                    'value' => 12
                ),
                'shuffle' => array(
                    'type' => 'checkbox',
                    'label' => 'mischen, um die Artikel in einer zufälligen Reihenfolge anzeigen zu lassen'
                )
            )
        );

    }

    public function parse($sContent = '', $aParams = array())
    {
        // Anzahl Artikel
        if(!$aParams['categoryid']){
            return null;
        }
        $Anzahl = $aParams['count'];
        // Bestimme Artikel aus Kategorie, bekomme ein Array mit Ergebnissen zurück
        $arrArtikel = $this->_getArticleId( $aParams);

        // Mische Array, um zufällige Produkte zu zeigen
        if($aParams['shuffle']){
            shuffle($arrArtikel);
            //die('SHUFFLE');
        }


        // Für die angegebene Anzahl werden Variablen mit Artikel-ID befüllt
        for( $i=0; $i<$Anzahl; $i++ ){
            ${'variable'.$i} = $arrArtikel[$i];
        }


        $uniqueId = md5( uniqid() );

        $j = 0;
        $row = 0;
        $display = '';
        $class= '';
        $more = false;

        $anzahl = 0;

        $sHTML = '';

        //TODO: Mobile Variante: Zeige nur Hälfte der Sachen


        while( ${'variable'.(6*$j)} != '' && $j<50){

            //mobil-Klassen:
            if($j==1){
                //$display = 'style="display:none"';
                $moreLittle = true;
                $class= 'hidden-xs hidden-sm';
            }
            //mobil-Klassen:

            if($j>1 && !$more){
                //$display = 'style="display:none"';
                $more = true;
                $class= 'hidden-xs hidden-sm hidden-md hidden-lg';
            }
            $sHTML .=  '<div id="'.$uniqueId.'catCarousel'. $row.'" class="boxwrapper '.$uniqueId.'catCarousel '.$class.'" '.$display.'>   
                            <div class="list-container">
                                <div class="row gridView newItems">';



            for($k = 6*$j;$k<(6*$j)+6 && ${'variable'.$k} != '';$k++){
                $sHTML .=               '<div class="productData productBox  col-xs-12 col-sm-6 col-md-2 listitem-type-grid">
                                            <div class="innerProductBox">
                                            [{oxid_include_widget cl="oxwArticleBox" _parent=$oView->getClassName() _navurlparams=$oViewConf->getNavUrlParams() anid="' . ${'variable'.$k} . '"  isVatIncluded=$oView->isVatIncluded() nocookie=1 sWidgetType=product sListType="listitem_grid" inlist=1 skipESIforUser=1}]
                                            </div>
                                         </div>';
                $anzahl++;
            }
            $sHTML .=            '</div>
                            </div>
                        </div>';

            $j++;
            $row++;

        }
        if($anzahl>24){
            $anzahlLittle = 24;
            $rowLittle = 4;
        } else {
            $anzahlLittle = $anzahl;
            $rowLittle = $row;
        }
        // TODO: Anzeige gezeigte Artikel/versteckte Artikel
        if($more) {
            $sHTML .= '<div id="'.$uniqueId.'more" class="container-fluid hidden-xs hidden-sm" style="text-align:center; padding-top:15px;">
                        <span id ="'.$uniqueId.'spanCount" style="font-size:12px;">12/' . $anzahl . '</span><br>
                        <button class="btn btn-primary" style="margin-top:5px;" onclick="loadCat(2,' . $row . ',\''.$uniqueId.'\')">MEHR LADEN</button>
                       </div>';
        }
        if($moreLittle){
            $sHTML.= '<div id="'.$uniqueId.'moreLittle" class="container-fluid hidden-md hidden-lg" style="text-align:center;">
                        <span id ="'.$uniqueId.'spanCountLittle" style="font-size:12px;">6/'.$anzahlLittle.'</span><br>
                        <button class="btn btn-primary" style="margin-top:5px;" onclick="loadCatLittle(1,' . $rowLittle . ',\''.$uniqueId.'\')">MEHR LADEN</button>
                       </div>';
        }





        $sHTML .= '<script type="text/javascript">
                        function loadCat(x,y,z){
                            
                            $("#"+z+"catCarousel" + x).removeClass("hidden-lg hidden-md");
                            $("#"+z+"catCarousel" + (x+1)).removeClass("hidden-lg hidden-md");
                            //$("#catCarousel" + (x+1)).show();
                            
                            $("#"+z+"more > .btn").attr(\'onclick\',\'loadCat(\'+(x+2)+\',\'+y+\',"\'+z+\'")\');
                            
                            if(x === y || (x+1) === y || (x+2) === y){
                                $("#"+z+"more").hide();
                            }
                            var count = 0;
                            $(".row ."+z+"catCarousel").each(function(){
                                //console.log($(this)[0].className.includes("hidden-lg"));
                                if(!$(this)[0].className.includes("hidden-lg")){
                                    var id = $(this)[0].id;
                                    $("#"+id + " .productData").each(function(){
                                        count++;
                                    })                                    
                                }
                            });
                            
                            //console.log($("#"+z+"spanCount")[0].innerHTML);
                            $("#"+z+"spanCount")[0].innerHTML = count+"/"+'.$anzahl.';
                            //$(\'.hiddenCat\').show();
                            //$(\'#more\').hide();
                        }
                        function loadCatLittle(x,y,z){
                            $("#"+z+"catCarousel" + x).removeClass("hidden-xs hidden-sm");
                            //$("#catCarousel" + (x+1)).removeClass("hidden-lg hidden-md");
                            //$("#catCarousel" + (x+1)).show();
                            
                            $("#"+z+"moreLittle > .btn").attr(\'onclick\',\'loadCatLittle(\'+(x+1)+\',\'+y+\',"\'+z+\'")\');
                            
                            if(x === y || (x+1) === y){
                                $("#"+z+"moreLittle").hide();
                            }
                            var count = 0;
                            $(".row ."+z+"catCarousel").each(function(){
                                //console.log($(this)[0].className.includes("hidden-xs"));
                                if(!$(this)[0].className.includes("hidden-xs")){
                                    var id = $(this)[0].id;
                                    $("#"+id + " .productData").each(function(){
                                        count++;
                                    })                                    
                                }
                            });
                            //console.log($("#"+z+"spanCountLittle")[0].innerHTML);
                            $("#"+z+"spanCountLittle")[0].innerHTML = count+"/"+'.$anzahlLittle.';
                            //$(\'.hiddenCat\').show();
                            //$(\'#more\').hide();
                        }
                    </script>';

        return $sHTML;
    }



    public function searchArticle()
    {
        /** @var \OxidEsales\Eshop\Core\Config $oConfig */
        $oConfig = $this->getConfig();

        $aArticles = array();

        if( $oConfig->getRequestParameter( 'value' ) )
        {
            ///** @var Article $oProduct
            $oProduct = oxNew( Article::class );
            $oProduct->load( $oConfig->getRequestParameter( 'value' ) );

            $aArticles[] = array(
                'value'       => $oProduct->getId(),
                'label'       => $oProduct->oxarticles__oxtitle->value,
                'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                'icon'        => $oProduct->getIconUrl()
            );

        }
        elseif( $oConfig->getRequestParameter( 'search' ) )
        {
            $oDb = DatabaseProvider::getDb();
            $sSearch = $oConfig->getRequestParameter( 'search' );

            /** @var ArticleList $oList */
            $oList = oxNew( ArticleList::class );
            $sViewName = getViewName( 'oxarticles' );

            $sSelect = "SELECT
                            *
                        FROM " . $sViewName . "
                        WHERE (
                            `OXARTNUM` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . " OR
                            `OXTITLE` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . " OR
                            `OXEAN` LIKE " . $oDb->quote( "%" . $sSearch . "%" ) . "
                          )
                          AND `OXPARENTID` = ''
                        ";

            $oList->selectString( $sSelect );

            /** @var Article $oProduct */
            foreach( $oList as $oProduct )
            {
                $aArticles[] = array(
                    'value'       => $oProduct->getId(),
                    'label'       => $oProduct->oxarticles__oxtitle->value,
                    'description' => 'Art-Nr.: ' . $oProduct->oxarticles__oxartnum->value . ( $oProduct->oxarticles__oxean->value ? ' / EAN: ' . $oProduct->oxarticles__oxean->value : '' ),
                    'icon'        => $oProduct->getIconUrl()
                );
            }
        }

        header( 'Content-Type: application/json' );
        Registry::getUtils()->showJsonAndExit( $aArticles );
    }

    protected function _getArticleId( $aParams = array() )
    {
        if( !$aParams )
        {
            return null;
        }
        $arrArticleId = $this->_findArticle( $aParams );
        return $arrArticleId;
    }

    protected function _findArticle( $aParams )
    {
        $oDb = DatabaseProvider::getDb( DatabaseProvider::FETCH_MODE_ASSOC );

        // Artikel mit gesetztem Flag aus den angegebenen Kategorien raussuchen

        $sortSQL = "SELECT OXDEFSORT,OXDEFSORTMODE FROM oxcategories WHERE OXID = ?";
        $aOne = $oDb->getRow($sortSQL,array($aParams['categoryid']));

        if($aOne['OXDEFSORTMODE'] == 0){
            $ascdesc = 'asc';
        } else {
            $ascdesc ='desc';
        }

        $sSQL = "SELECT
                     oa.oxid,
                     oa.oxstock,
                     oa.oxvarstock
                 FROM " . getViewName( 'oxarticles' ) . " AS oa
                     INNER JOIN oxobject2category AS o2c ON o2c.oxobjectid = oa.oxid
                     INNER JOIN oxcategories AS oc ON oc.oxid = o2c.oxcatnid
                 WHERE oa.oxactive = 1";

        // Kategorie
        if( $aParams[ 'categoryid' ] )
        {
            $sSQL .= " AND oc.oxid = " . $oDb->quote( $aParams[ 'categoryid' ] );
        }
        if($aOne['OXDEFSORT'] !== ""){
            $sSQL .= " ORDER BY oa." . $aOne['OXDEFSORT'] . " " . $ascdesc;
        } else {
            $sSQL .= " ORDER BY o2c.OXPOS ASC";
        }

        $aAll = $oDb->getAll( $sSQL );

        // Überprüfen, ob für den Artikel der Mindestbestand erreicht wird
        // Bei Vaterartikeln muss die Summe der Lagermengen der Varianten berücksichtigt werden
        $aSelected = Array();

        foreach ( $aAll AS $aArticle )
        {
            //  if ( $aArticle[ 'OXSTOCK' ] + $aArticle[ 'OXVARSTOCK' ] > (int)$aParams[ 'minstock' ] )
            // {
            $aSelected[] = $aArticle[ 'OXID' ];
            // }
        }
        return $aSelected;
    }
}